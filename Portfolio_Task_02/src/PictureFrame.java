import java.awt.*;

import javax.swing.*;

public class PictureFrame {
  public int[] reroll = null;
  Aardvark master = null;

  class DominoPanel extends JPanel {
    private static final long serialVersionUID = 4190229282411119364L;

    public void drawGrid(Graphics g) {
      for (int are = 0; are < 7; are++) {
        for (int see = 0; see < 8; see++) {
          drawDigitGivenCentre(new DrawDigitGivenCentreParameter(g, 30 + see * 20, 30 + are * 20, 20, master.data.grid[are][see]));
        }
      }
    }

    public void drawGridLines(Graphics g) {
      g.setColor(Color.LIGHT_GRAY);
      for (int are = 0; are <= 7; are++) {
        g.drawLine(20, 20 + are * 20, 180, 20 + are * 20);
      }
      for (int see = 0; see <= 8; see++) {
        g.drawLine(20 + see * 20, 20, 20 + see * 20, 160);
      }
    }

    public void drawHeadings(Graphics g) {
      for (int are = 0; are < 7; are++) {
        fillDigitGivenCentre(new FillDigitGivenCentreParameter(g, 10, 30 + are * 20, 20, are+1));
      }

      for (int see = 0; see < 8; see++) {
        fillDigitGivenCentre(new FillDigitGivenCentreParameter(g, 30 + see * 20, 10, 20, see+1));
      }
    }

    public void drawDomino(Graphics g, Domino d) {
      if (d.placed) {
        int y = Math.min(d.ly, d.hy);
        int x = Math.min(d.lx, d.hx);
        int w = Math.abs(d.lx - d.hx) + 1;
        int h = Math.abs(d.ly - d.hy) + 1;
        g.setColor(Color.WHITE);
        g.fillRect(20 + x * 20, 20 + y * 20, w * 20, h * 20);
        g.setColor(Color.RED);
        g.drawRect(20 + x * 20, 20 + y * 20, w * 20, h * 20);
        drawDigitGivenCentre(g, 30 + d.hx * 20, 30 + d.hy * 20, 20, d.high,
            Color.BLUE);
        drawDigitGivenCentre(g, 30 + d.lx * 20, 30 + d.ly * 20, 20, d.low,
            Color.BLUE);
      }
    }

    void drawDigitGivenCentre(DrawDigitGivenCentreParameter parameterObject) {
      int radius = parameterObject.diameter / 2;
      parameterObject.g.setColor(Color.BLACK);
      
      extracted(parameterObject.g, parameterObject.x, parameterObject.y, parameterObject.n);
    }

	private void extracted(Graphics g, int x, int y, int n) {
		FontMetrics fm = g.getFontMetrics();
		  String txt = Integer.toString(n);
		  g.drawString(txt, x - fm.stringWidth(txt) / 2, y + fm.getMaxAscent() / 2);
	}

    void drawDigitGivenCentre(Graphics g, int x, int y, int diameter, int n,
        Color c) {
      int radius = diameter / 2;
      g.setColor(c);
  
      extracted(g, x, y, n);
    }

    void fillDigitGivenCentre(FillDigitGivenCentreParameter parameterObject) {
      int radius = parameterObject.diameter / 2;
      parameterObject.g.setColor(Color.GREEN);
      parameterObject.g.fillOval(parameterObject.x - radius, parameterObject.y - radius, parameterObject.diameter, parameterObject.diameter);
      parameterObject.g.setColor(Color.BLACK);
      parameterObject.g.drawOval(parameterObject.x - radius, parameterObject.y - radius, parameterObject.diameter, parameterObject.diameter);
      extracted(parameterObject.g, parameterObject.x, parameterObject.y, parameterObject.n);
    }

    protected void paintComponent(Graphics g) {
      g.setColor(Color.YELLOW);
      g.fillRect(0, 0, getWidth(), getHeight());

      
      if (master.data.mode == 1) {
        drawGridLines(g);
        drawHeadings(g);
        drawGrid(g);
        master.drawGuesses(g);
      }
      if (master.data.mode == 0) {
        drawGridLines(g);
        drawHeadings(g);
        drawGrid(g);
        master.drawDominoes(g);
      }
    }

    public Dimension getPreferredSize() {
      return new Dimension(202, 182);
    }
  }

  public DominoPanel dp;

  public void PictureFrame(Aardvark sf) {
    master = sf;
    if (dp == null) {
      JFrame f = new JFrame("Abominodo");
      dp = new DominoPanel();
      f.setContentPane(dp);
      f.pack();
      f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
      f.setVisible(true);
    }
  }

  public void reset() {
    // TODO Auto-generated method stub

  }

}

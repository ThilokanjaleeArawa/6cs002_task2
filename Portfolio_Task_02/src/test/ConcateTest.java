package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ConcateTest {

	@Test
	public void testConcatenate() {
		JUnit test =  new JUnit();
		String result = test.concatenate("one", "two");
		assertEquals("onetwo", result);
	
	}

}

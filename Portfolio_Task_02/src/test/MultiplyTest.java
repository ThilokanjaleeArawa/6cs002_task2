package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MultiplyTest {

	@Test
		public void testMultiply() {
			JUnit test =  new JUnit();
			int result = test.multiply(3, 4);
			assertEquals(12, result);
		
	}

}
